package configs

import (
	"fmt"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"leboncoin.tech/test/internal/commons/environments"

	"gopkg.in/yaml.v2"
	"leboncoin.tech/test/internal/commons/messages"
)

type Config struct {
	Server struct {
		Port string `yaml:"port"`
		Env  string `yaml:"environments"`
	}
	Database struct {
		URI        string `yaml:"uri"`
		Name       string `yaml:"name"`
		Collection string `yaml:"collection"`
	}
}

func (c *Config) load() {
	var env string
	var path string

	root, dirErr := os.Getwd()
	if dirErr != nil {
		log.Fatalln(messages.CannotGetWd)
	}

	env = os.Getenv("APP_ENV")
	switch env {
	case environments.Prod:
		path = filepath.Join(root, fmt.Sprintf("%s.config.yml", environments.Prod))
	case environments.Dev:
		path = filepath.Join(root, "internal", "configs", fmt.Sprintf("%s.config.yml", environments.Dev))
	case environments.Test:
		path = filepath.Join(root, "..", "configs", fmt.Sprintf("%s.config.yml", environments.Test))
	default:
		path = filepath.Join(root, "internal", "configs", fmt.Sprintf("%s.config.yml", environments.Dev))
	}

	f, err := os.Open(path)
	if err != nil {
		log.Fatalln(messages.CannotOpenFile)
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Error(messages.CannotCloseFile)
		}
	}(f)

	parseErr := yaml.NewDecoder(f).Decode(c)
	if parseErr != nil {
		log.Error(messages.CannotLoadConfiguration)
	}
}

func New() *Config {
	c := &Config{}
	c.load()

	return c
}
