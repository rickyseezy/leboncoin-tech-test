package rest

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"leboncoin.tech/test/internal/commons/messages"
	"leboncoin.tech/test/internal/domain/usecases"
	"leboncoin.tech/test/internal/interfaces/rest/middlewares"
)

type ServerEngine interface {
	Start()
}

type Server struct {
	router              *gin.Engine
	port                string
	fizzBuzzApplication usecases.FizzBuzz
}

func (s *Server) Start() {
	s.configure()

	err := s.router.Run(fmt.Sprintf(":%s", s.port))
	if err != nil {
		log.Fatal(errors.Wrap(err, messages.CannotStartServer))
	}
}

//nolint:gocritic // router configuration
func (s *Server) configure() {
	s.router.Use(middlewares.Cors())
	s.router.GET("/healthz", s.healthz)

	fizzbuzz := s.router.Group("/fizzbuzz")
	{
		fizzbuzz.GET("", s.fizzbuzz)
		fizzbuzz.GET("/stats", s.stats)
	}
}

func New(r *gin.Engine, fizzBuzzApp usecases.FizzBuzz, p string) *Server {
	return &Server{
		router:              r,
		port:                p,
		fizzBuzzApplication: fizzBuzzApp,
	}
}
