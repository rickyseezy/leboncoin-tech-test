package rest

import (
	"net/http"

	log "github.com/sirupsen/logrus"
	"leboncoin.tech/test/internal/commons/messages"

	"github.com/gin-gonic/gin"
	"leboncoin.tech/test/internal/domain/models"
	"leboncoin.tech/test/internal/helpers"
)

const (
	QueryParamsInt1  = "int1"
	QueryParamsInt2  = "int2"
	QueryParamsStr1  = "str1"
	QueryParamsStr2  = "str2"
	QueryParamsLimit = "limit"
)

func (s *Server) healthz(c *gin.Context) {
	isHealthy := s.fizzBuzzApplication.Healthz()
	if isHealthy != nil {
		log.Error(messages.ServiceNotHealthy)
		c.JSON(http.StatusServiceUnavailable, &models.AppResponse{
			Code:    http.StatusServiceUnavailable,
			Message: "KO !",
		})
		return
	}

	c.JSON(http.StatusOK, &models.AppResponse{
		Code:    http.StatusOK,
		Message: "Ok !",
	})
}

func (s *Server) fizzbuzz(c *gin.Context) {
	int1 := helpers.ConvertStringToInt(c.Query(QueryParamsInt1))
	int2 := helpers.ConvertStringToInt(c.Query(QueryParamsInt2))
	str1 := c.Query(QueryParamsStr1)
	str2 := c.Query(QueryParamsStr2)
	limit := helpers.ConvertStringToInt(c.Query(QueryParamsLimit))

	fizzRequest := &models.FizzRequest{
		Int1:  int1,
		Int2:  int2,
		Str1:  str1,
		Str2:  str2,
		Limit: limit,
	}

	res, err := s.fizzBuzzApplication.Generate(fizzRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.AppResponse{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, &models.Response{
		Response: res,
	})
}

func (s *Server) stats(c *gin.Context) {
	res, isEmpty := s.fizzBuzzApplication.Stats()
	if isEmpty != nil {
		c.JSON(http.StatusNoContent, &models.AppResponse{
			Code:    http.StatusNoContent,
			Message: messages.NoStatsAvailable,
		})
		return
	}

	c.JSON(http.StatusOK, &models.ResponseWithStats{
		Response: res,
	})
}
