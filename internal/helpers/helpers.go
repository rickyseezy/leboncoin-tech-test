package helpers

import (
	"strconv"
	"time"

	"leboncoin.tech/test/internal/domain/models"
)

const InitQueryCount = 1

func MapRequestToModel(fr *models.FizzRequest) *models.FizzBuzz {
	return &models.FizzBuzz{
		Int1:      fr.Int1,
		Int2:      fr.Int2,
		Str1:      fr.Str1,
		Str2:      fr.Str2,
		Limit:     fr.Limit,
		Count:     InitQueryCount,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
}

func MapModelToQuery(fizzBuzz *models.FizzBuzz) *models.FizzBuzzQueryParams {
	return &models.FizzBuzzQueryParams{
		Int1:  fizzBuzz.Int1,
		Int2:  fizzBuzz.Int2,
		Str1:  fizzBuzz.Str1,
		Str2:  fizzBuzz.Str2,
		Limit: fizzBuzz.Limit,
	}
}

func ConvertStringToInt(str string) int {
	nb, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}

	return nb
}
