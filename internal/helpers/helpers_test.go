package helpers_test

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"leboncoin.tech/test/internal/domain/models"
	"leboncoin.tech/test/internal/helpers"
	"testing"
	"time"
)

type HelpersTestSuite struct {
	suite.Suite
}

func (suite *HelpersTestSuite) TestMapRequestToModel() {
	tests := []struct {
		Title    string
		Request  *models.FizzRequest
		Expected *models.FizzBuzz
	}{
		{
			Title: "Should map request to model with all params provided",
			Request: &models.FizzRequest{
				Int1:  2,
				Int2:  3,
				Str1:  "fizz",
				Str2:  "buzz",
				Limit: 3,
			},
			Expected: &models.FizzBuzz{
				Int1:      2,
				Int2:      3,
				Str1:      "fizz",
				Str2:      "buzz",
				Limit:     3,
				Count:     1,
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		},
		{
			Title: "Should map request to model with missing params",
			Request: &models.FizzRequest{
				Str1:  "foo",
				Str2:  "bar",
				Limit: 5,
			},
			Expected: &models.FizzBuzz{
				Str1:      "foo",
				Str2:      "bar",
				Limit:     5,
				Count:     1,
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		},
	}

	for _, tt := range tests {
		fizzBuzz := helpers.MapRequestToModel(tt.Request)
		fizzBuzz.CreatedAt = time.Time{}
		fizzBuzz.UpdatedAt = time.Time{}

		assert.Equal(suite.T(), tt.Expected, fizzBuzz)
	}
}

func (suite *HelpersTestSuite) TestMapModelToQuery() {
	tests := []struct {
		Title    string
		Model    *models.FizzBuzz
		Expected *models.FizzBuzzQueryParams
	}{
		{
			Title: "Should map model to query with all params provided",
			Model: &models.FizzBuzz{
				Int1:      2,
				Int2:      3,
				Str1:      "fizz",
				Str2:      "buzz",
				Limit:     3,
				Count:     1,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			Expected: &models.FizzBuzzQueryParams{
				Int1:  2,
				Int2:  3,
				Str1:  "fizz",
				Str2:  "buzz",
				Limit: 3,
			},
		},
		{
			Title: "Should map model to query with missing params",
			Model: &models.FizzBuzz{
				Str1:      "foo",
				Str2:      "bar",
				Limit:     5,
				Count:     1,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
			Expected: &models.FizzBuzzQueryParams{
				Str1:  "foo",
				Str2:  "bar",
				Limit: 5,
			},
		},
	}

	for _, tt := range tests {
		fizzBuzz := helpers.MapModelToQuery(tt.Model)

		assert.Equal(suite.T(), tt.Expected, fizzBuzz)
	}
}

func (suite *HelpersTestSuite) TestConvertStringToInt() {
	tests := []struct {
		title      string
		queryParam string
		expected   int
	}{
		{
			title:      "Should convert string to int with a valid value",
			queryParam: "1",
			expected:   1,
		},
		{
			title:      "Should not convert string to int with a invalid value",
			queryParam: "foo",
			expected:   0,
		},
	}

	for _, tt := range tests {
		nb := helpers.ConvertStringToInt(tt.queryParam)
		assert.Equal(suite.T(), tt.expected, nb, tt.title)
	}
}

func TestHelpersTestSuite(t *testing.T) {
	suite.Run(t, new(HelpersTestSuite))
}
