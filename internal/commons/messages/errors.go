package messages

const (
	DatabaseError           = "database error"
	DatabaseNetworkError    = "database network error"
	CannotDialServer        = "cannot dial server"
	CannotLoadConfiguration = "cannot load configuration"
	CannotCloseFile         = "cannot close file"
	CannotOpenFile          = "cannot open file"
	CannotStartServer       = "cannot start server"
	InvalidRequest          = "invalid request"
	CannotGetWd             = "cannot get current working directory"
	CannotCreateFizzBuzz    = "cannot create fizzbuzz"
	NoStatsAvailable        = "no stats available"
	ServiceNotHealthy       = "service not healthy"
)
