package application

import (
	"strconv"

	"github.com/pkg/errors"
	"leboncoin.tech/test/internal/commons/messages"

	"leboncoin.tech/test/internal/domain/models"
	"leboncoin.tech/test/internal/domain/repositories"
	"leboncoin.tech/test/internal/helpers"
)

type FizzBuzzApp struct {
	fizzBuzzRepo repositories.FizzBuzz
}

func (f *FizzBuzzApp) Generate(fizzBuzzRequest *models.FizzRequest) ([]string, error) {
	isValid := fizzBuzzRequest.Validate()
	if isValid != nil {
		return nil, errors.Wrap(isValid, messages.InvalidRequest)
	}

	err := f.create(fizzBuzzRequest)
	if err != nil {
		return nil, err
	}

	response := make([]string, 0)
	for i := 1; i <= fizzBuzzRequest.Limit; i++ {
		if i%fizzBuzzRequest.Int1 == 0 {
			response = append(response, fizzBuzzRequest.Str1)
			continue
		}
		if i%fizzBuzzRequest.Int2 == 0 {
			response = append(response, fizzBuzzRequest.Str2)
			continue
		}

		response = append(response, strconv.Itoa(i))
	}

	return response, nil
}

func (f *FizzBuzzApp) Stats() (*models.FizzBuzz, error) {
	res, empty := f.fizzBuzzRepo.Stats()
	if empty != nil {
		return nil, empty
	}

	return res, nil
}

func (f *FizzBuzzApp) create(fizzBuzzRequest *models.FizzRequest) error {
	err := f.fizzBuzzRepo.Create(helpers.MapRequestToModel(fizzBuzzRequest))
	if err != nil {
		return errors.Wrap(err, messages.CannotCreateFizzBuzz)
	}

	return nil
}

func (f *FizzBuzzApp) Healthz() error {
	err := f.fizzBuzzRepo.Healthz()
	if err != nil {
		return errors.Wrap(err, messages.CannotDialServer)
	}

	return nil
}

func New(repo repositories.FizzBuzz) *FizzBuzzApp {
	return &FizzBuzzApp{
		fizzBuzzRepo: repo,
	}
}
