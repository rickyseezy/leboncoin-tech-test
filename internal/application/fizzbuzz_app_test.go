package application_test

import (
	"github.com/globalsign/mgo"
	"github.com/golang/mock/gomock"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"leboncoin.tech/test/internal/application"
	"leboncoin.tech/test/internal/commons/messages"
	"leboncoin.tech/test/internal/configs"
	"leboncoin.tech/test/internal/datasources"
	"leboncoin.tech/test/internal/domain/models"
	"leboncoin.tech/test/internal/domain/repositories"
	mocks_fizzbuzz_repo "leboncoin.tech/test/internal/domain/repositories/mocks"
	"leboncoin.tech/test/internal/domain/usecases"
	"testing"
	"time"
)

// dummy data
var date = time.Now()
var fizzBuzzMocks = []*models.FizzBuzz{
	{
		Int1:      3,
		Int2:      5,
		Str1:      "fizz",
		Str2:      "buzz",
		Limit:     10,
		Count:     3,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Int1:      2,
		Int2:      7,
		Str1:      "foo",
		Str2:      "bar",
		Limit:     5,
		Count:     2,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Int1:      6,
		Int2:      3,
		Str1:      "star",
		Str2:      "lbc",
		Limit:     3,
		Count:     9,
		CreatedAt: date,
		UpdatedAt: date,
	},
	{
		Int1:      20,
		Int2:      15,
		Str1:      "sara",
		Str2:      "croche",
		Limit:     3,
		Count:     9,
		CreatedAt: date.AddDate(0, 0, -1),
		UpdatedAt: date.AddDate(0, 0, -1),
	},
}

type FizzBuzzAppTestSuite struct {
	suite.Suite
	fizzBuzzRepo repositories.FizzBuzz
	fizzBuzzApp  usecases.FizzBuzz
	collection   *mgo.Collection
}

func (suite *FizzBuzzAppTestSuite) initDB() {
	// init db with dummy data
	for _, mock := range fizzBuzzMocks {
		err := suite.fizzBuzzRepo.Create(mock)
		if err != nil {
			log.Fatalln("could not create mock data")
		}
	}
}

func (suite *FizzBuzzAppTestSuite) clearDB() {
	_ = suite.collection.DropCollection()
}

func (suite *FizzBuzzAppTestSuite) SetupTest() {
	c := configs.New()

	session, err := mgo.Dial(c.Database.URI)
	if err != nil {
		log.Fatalln(errors.Wrap(err, messages.DatabaseNetworkError))
	}
	// this is use to clear the collection when the tests are done
	collec := session.DB(c.Database.Name).C(c.Database.Collection)

	suite.collection = collec
	suite.fizzBuzzRepo = datasources.New(session, c)
	suite.fizzBuzzApp = application.New(suite.fizzBuzzRepo)
}

func (suite *FizzBuzzAppTestSuite) TestGenerate() {
	ctrl := gomock.NewController(suite.T())
	defer ctrl.Finish()

	tests := []struct {
		title                string
		request              *models.FizzRequest
		expected             []string
		expectedErrorMessage string
		wantErr              bool
	}{
		{
			title: "Should return a list of 10 strings",
			request: &models.FizzRequest{
				Int1:  3,
				Int2:  5,
				Str1:  "fizz",
				Str2:  "buzz",
				Limit: 10,
			},
			expected: []string{"1", "2", "fizz", "4", "buzz", "fizz", "7", "8", "fizz", "buzz"},
			wantErr:  false,
		},
		{
			title: "Should return a list of 5 strings",
			request: &models.FizzRequest{
				Int1:  2,
				Int2:  4,
				Str1:  "foo",
				Str2:  "bar",
				Limit: 5,
			},
			expected: []string{"1", "foo", "3", "foo", "5"},
			wantErr:  false,
		},
		{
			title: "Should not return a response when params are missing",
			request: &models.FizzRequest{
				Int1: 2,
				Int2: 4,
			},
			expectedErrorMessage: "invalid request: Limit: cannot be blank; Str1: cannot be blank; Str2: cannot be blank.",
			wantErr:              true,
		},
	}

	fizzBuzzRepo := mocks_fizzbuzz_repo.NewMockFizzBuzz(ctrl)
	fizzBuzzApp := application.New(fizzBuzzRepo)

	for _, tt := range tests {
		if !tt.wantErr {
			fizzBuzzRepo.
				EXPECT().
				Create(gomock.Any()).
				Return(nil)
		}

		res, err := fizzBuzzApp.Generate(tt.request)
		if tt.wantErr {
			assert.NotNil(suite.T(), err)
			assert.Equal(suite.T(), tt.expectedErrorMessage, err.Error())
			assert.Nil(suite.T(), res)
			continue
		}

		assert.Equal(suite.T(), tt.expected, res, tt.title)
		assert.Nil(suite.T(), err)
	}
}

func (suite *FizzBuzzAppTestSuite) TestStatsWithData() {
	// init db with dummy data
	suite.initDB()

	tests := []struct {
		title    string
		expected *models.FizzBuzz
	}{
		{
			title: "Should return the most used request",
			expected: &models.FizzBuzz{
				Int1:      6,
				Int2:      3,
				Str1:      "star",
				Str2:      "lbc",
				Limit:     3,
				Count:     9,
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		},
	}

	for _, tt := range tests {
		res, err := suite.fizzBuzzApp.Stats()
		tt.expected.ID = res.ID // As we can't use predictive ID we have to bind it for test purpose
		res.CreatedAt = time.Time{}
		res.UpdatedAt = time.Time{}

		assert.Nil(suite.T(), err)
		assert.Equal(suite.T(), tt.expected, res, tt.title)
	}

	suite.clearDB()
}

func (suite *FizzBuzzAppTestSuite) TestStatsWithEmptyData() {
	res, err := suite.fizzBuzzApp.Stats()
	assert.NotNil(suite.T(), err)
	assert.Nil(suite.T(), res)
	assert.Equal(suite.T(), "no stats available: not found", err.Error())
}

func (suite *FizzBuzzAppTestSuite) TestHealthzWhenServerIsRunning() {
	err := suite.fizzBuzzRepo.Healthz()

	assert.Nil(suite.T(), err)
}

func (suite *FizzBuzzAppTestSuite) TestHealthzWhenServerIsNotRunning() {
	ctrl := gomock.NewController(suite.T())
	defer ctrl.Finish()

	fizzBuzzRepo := mocks_fizzbuzz_repo.NewMockFizzBuzz(ctrl)
	fizzBuzzApp := application.New(fizzBuzzRepo)
	// Simulate server closed connexion
	fizzBuzzRepo.
		EXPECT().
		Healthz().
		Return(errors.New(messages.DatabaseNetworkError))

	err := fizzBuzzApp.Healthz()

	assert.NotNil(suite.T(), err)
}

func TestFizzBuzzAppTestSuite(t *testing.T) {
	suite.Run(t, new(FizzBuzzAppTestSuite))
}
