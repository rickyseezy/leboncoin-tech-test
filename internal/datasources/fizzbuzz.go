package datasources

import (
	"time"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"leboncoin.tech/test/internal/commons/messages"
	"leboncoin.tech/test/internal/configs"
	"leboncoin.tech/test/internal/helpers"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"leboncoin.tech/test/internal/domain/models"
)

type FizzBuzz struct {
	session    *mgo.Session
	collection *mgo.Collection
}

func (f *FizzBuzz) Stats() (*models.FizzBuzz, error) {
	var result *models.FizzBuzz

	notFound := f.collection.Find(bson.M{}).Sort("-count", "-created_at").One(&result)
	if notFound != nil {
		log.Error(notFound)
		return nil, errors.Wrap(notFound, messages.NoStatsAvailable)
	}

	return result, nil
}

func (f *FizzBuzz) Create(fizzBuzz *models.FizzBuzz) error {
	fizzBuzzQueryParams := helpers.MapModelToQuery(fizzBuzz)
	request, notFound := f.FindWithParams(fizzBuzzQueryParams)
	if notFound != nil {
		log.Error(notFound)

		err := f.collection.Insert(fizzBuzz)
		if err != nil {
			log.Error(err)
			return errors.Wrap(err, messages.DatabaseError)
		}
		return nil
	}

	err := f.UpdateCounter(request.ID, request.Count)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (f *FizzBuzz) UpdateCounter(fizzBuzzID bson.ObjectId, fizzBuzzCount int) error {
	err := f.collection.Update(bson.M{"_id": fizzBuzzID}, bson.M{
		"$set": bson.M{
			"count":      fizzBuzzCount + 1,
			"updated_at": time.Now(),
		},
	})
	if err != nil {
		log.Error(err)
		return errors.Wrap(err, messages.DatabaseError)
	}

	return nil
}

func (f *FizzBuzz) FindWithParams(fizzBuzzQueryParams *models.FizzBuzzQueryParams) (*models.FizzBuzz, error) {
	var result *models.FizzBuzz

	notFound := f.collection.Find(bson.M{
		"int1":  fizzBuzzQueryParams.Int1,
		"int2":  fizzBuzzQueryParams.Int2,
		"str1":  fizzBuzzQueryParams.Str1,
		"str2":  fizzBuzzQueryParams.Str2,
		"limit": fizzBuzzQueryParams.Limit,
	}).One(&result)
	if notFound != nil {
		log.Error(notFound)
		return nil, errors.Wrap(notFound, messages.DatabaseError)
	}

	return result, nil
}

func (f *FizzBuzz) Healthz() error {
	err := f.session.Ping()
	if err != nil {
		log.Error(err)
		return errors.Wrap(err, messages.DatabaseNetworkError)
	}

	return nil
}

func New(s *mgo.Session, cfg *configs.Config) *FizzBuzz {
	c := s.DB(cfg.Database.Name).C(cfg.Database.Collection)

	return &FizzBuzz{
		session:    s,
		collection: c,
	}
}
