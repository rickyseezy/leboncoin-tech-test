package datasources_test

import (
	"github.com/globalsign/mgo"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"leboncoin.tech/test/internal/commons/messages"
	"leboncoin.tech/test/internal/configs"
	"leboncoin.tech/test/internal/datasources"
	"leboncoin.tech/test/internal/domain/models"
	"leboncoin.tech/test/internal/domain/repositories"
	"leboncoin.tech/test/internal/helpers"
	"testing"
	"time"
)

// dummy data
var date = time.Now()
var fizzBuzzMocks = []*models.FizzBuzz{
	{
		Int1:      18,
		Int2:      10,
		Str1:      "toto",
		Str2:      "tutu",
		Limit:     30,
		Count:     9,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Int1:      6,
		Int2:      20,
		Str1:      "lbc",
		Str2:      ".com",
		Limit:     25,
		Count:     30,
		CreatedAt: date,
		UpdatedAt: date,
	},
	{
		Int1:      28,
		Int2:      53,
		Str1:      "aaaa",
		Str2:      "bbbb",
		Limit:     60,
		Count:     13,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Int1:      11,
		Int2:      7,
		Str1:      "sara",
		Str2:      "croche",
		Limit:     20,
		Count:     29,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
}

type FizzBuzzRepoTestSuite struct {
	suite.Suite
	fizzBuzzRepo repositories.FizzBuzz
	collection   *mgo.Collection
	session      *mgo.Session
}

func (suite *FizzBuzzRepoTestSuite) initDB() {
	// init db with dummy data
	for _, mock := range fizzBuzzMocks {
		err := suite.fizzBuzzRepo.Create(mock)
		if err != nil {
			log.Fatalln("could not create mock data")
		}
	}
}

func (suite *FizzBuzzRepoTestSuite) clearDB() {
	_ = suite.collection.DropCollection()
}

func (suite *FizzBuzzRepoTestSuite) SetupTest() {
	c := configs.New()

	session, err := mgo.Dial(c.Database.URI)
	if err != nil {
		log.Fatalln(messages.DatabaseNetworkError)
	}
	// this is use to clear the collection when the tests are done
	collec := session.DB(c.Database.Name).C(c.Database.Collection)

	suite.session = session
	suite.collection = collec
	suite.fizzBuzzRepo = datasources.New(session, c)
}

func (suite *FizzBuzzRepoTestSuite) TestStatsWhenNoDataAvailable() {
	fizzBuzz, err := suite.fizzBuzzRepo.Stats()

	assert.NotNil(suite.T(), err)
	assert.Nil(suite.T(), fizzBuzz)
	assert.Equal(suite.T(), "no stats available: not found", err.Error())
}

func (suite *FizzBuzzRepoTestSuite) TestStatsWhenDataAreAvailable() {
	suite.initDB()

	tests := []struct {
		title    string
		expected *models.FizzBuzz
	}{
		{
			title: "Should return the most used request",
			expected: &models.FizzBuzz{
				Int1:      6,
				Int2:      20,
				Str1:      "lbc",
				Str2:      ".com",
				Limit:     25,
				Count:     30,
				CreatedAt: time.Time{},
				UpdatedAt: time.Time{},
			},
		},
	}

	for _, tt := range tests {
		res, err := suite.fizzBuzzRepo.Stats()
		tt.expected.ID = res.ID // As we can't use predictive ID we have to bind it for test purpose
		res.CreatedAt = time.Time{}
		res.UpdatedAt = time.Time{}

		assert.Nil(suite.T(), err)
		assert.Equal(suite.T(), tt.expected, res, tt.title)
	}

	suite.clearDB()
}

func (suite *FizzBuzzRepoTestSuite) TestCreate() {
	tests := []struct {
		title string
		data  *models.FizzBuzz
	}{
		{
			title: "Should create a request",
			data: &models.FizzBuzz{
				Int1:      3,
				Int2:      2,
				Str1:      "foo",
				Str2:      "bar",
				Limit:     10,
				Count:     1,
				CreatedAt: time.Now(),
				UpdatedAt: time.Now(),
			},
		},
	}

	for _, tt := range tests {
		err := suite.fizzBuzzRepo.Create(tt.data)
		assert.Nil(suite.T(), err, tt.title)
	}

	suite.clearDB()
}

func (suite *FizzBuzzRepoTestSuite) TestUpdateCounter() {
	suite.initDB()

	tests := []struct {
		title    string
		data     *models.FizzBuzz
		expected int
	}{
		{
			title:    "Should update the first request counter",
			data:     fizzBuzzMocks[0],
			expected: 10,
		},
		{
			title:    "Should update the third request counter",
			data:     fizzBuzzMocks[2],
			expected: 14,
		},
	}

	for _, tt := range tests {
		// simulate multiple query calls
		fizzBuzz, _ := suite.fizzBuzzRepo.FindWithParams(helpers.MapModelToQuery(tt.data))
		_ = suite.fizzBuzzRepo.Create(fizzBuzz)
		updatedFizzBuzz, _ := suite.fizzBuzzRepo.FindWithParams(helpers.MapModelToQuery(tt.data))

		assert.Equal(suite.T(), tt.expected, updatedFizzBuzz.Count, tt.title)
	}

	suite.clearDB()
}

func (suite *FizzBuzzRepoTestSuite) TestFindWithParams() {
	suite.initDB()

	tests := []struct {
		title                string
		query                *models.FizzBuzzQueryParams
		wantErr              bool
		expectedErrorMessage string
	}{
		{
			title:   "Should find the request",
			query:   helpers.MapModelToQuery(fizzBuzzMocks[0]),
			wantErr: false,
		},
		{
			title: "Should not find the request",
			query: &models.FizzBuzzQueryParams{
				Int1:  40,
				Int2:  60,
				Str1:  "bad",
				Str2:  "query",
				Limit: 2,
			},
			wantErr:              true,
			expectedErrorMessage: "database error: not found",
		},
	}

	for _, tt := range tests {
		fizzBuzz, notFound := suite.fizzBuzzRepo.FindWithParams(tt.query)
		if tt.wantErr {
			assert.NotNil(suite.T(), notFound, tt.title)
			assert.Nil(suite.T(), fizzBuzz, tt.title)
			assert.Equal(suite.T(), tt.expectedErrorMessage, notFound.Error())
			continue
		}

		assert.Nil(suite.T(), notFound, tt.title)
		assert.NotNil(suite.T(), fizzBuzz, tt.title)
	}

	suite.clearDB()
}

func (suite *FizzBuzzRepoTestSuite) TestHealthz() {
	err := suite.fizzBuzzRepo.Healthz()

	assert.Nil(suite.T(), err)
}

func TestFizzBuzzRepoTestSuite(t *testing.T) {
	suite.Run(t, new(FizzBuzzRepoTestSuite))
}
