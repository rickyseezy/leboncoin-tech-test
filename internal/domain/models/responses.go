package models

type AppResponse struct {
	Code    int64  `json:"code"`
	Message string `json:"message"`
}

type Response struct {
	Response []string `json:"response"`
}

type ResponseWithStats struct {
	Response *FizzBuzz `json:"response"`
}
