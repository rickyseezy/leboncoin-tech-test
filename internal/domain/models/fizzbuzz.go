package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type FizzBuzz struct {
	ID        bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Int1      int           `bson:"int1"`
	Int2      int           `bson:"int2"`
	Str1      string        `bson:"str1"`
	Str2      string        `bson:"str2"`
	Limit     int           `bson:"limit"`
	Count     int           `bson:"count"`
	CreatedAt time.Time     `bson:"created_at"`
	UpdatedAt time.Time     `bson:"updated_at"`
}

type FizzBuzzQueryParams struct {
	Int1  int
	Int2  int
	Str1  string
	Str2  string
	Limit int
}
