package models

import validation "github.com/go-ozzo/ozzo-validation"

type FizzRequest struct {
	Int1  int
	Int2  int
	Str1  string
	Str2  string
	Limit int
}

func (f FizzRequest) Validate() error {
	return validation.ValidateStruct(&f,
		validation.Field(&f.Int1, validation.Required, validation.Min(1)),
		validation.Field(&f.Int2, validation.Required, validation.Min(1)),
		validation.Field(&f.Str1, validation.Required, validation.Length(1, 15)),
		validation.Field(&f.Str2, validation.Required, validation.Length(1, 15)),
		validation.Field(&f.Limit, validation.Required, validation.Min(1), validation.Max(100)),
	)
}
