package repositories

import (
	"github.com/globalsign/mgo/bson"
	"leboncoin.tech/test/internal/domain/models"
)

//go:generate mockgen -destination=mocks/mock_fizzbuzz_repo.go -package=mocks_fizzbuzz_repo . FizzBuzz
type FizzBuzz interface {
	FindWithParams(fizzBuzzQueryParams *models.FizzBuzzQueryParams) (*models.FizzBuzz, error)
	Stats() (*models.FizzBuzz, error)
	Create(fizzBuzz *models.FizzBuzz) error
	UpdateCounter(fizzBuzzID bson.ObjectId, fizzBuzzCount int) error
	Healthz() error
}
