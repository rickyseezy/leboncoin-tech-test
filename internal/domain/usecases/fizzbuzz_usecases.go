package usecases

import "leboncoin.tech/test/internal/domain/models"

type FizzBuzz interface {
	Generate(fizzBuzzRequest *models.FizzRequest) ([]string, error)
	Stats() (*models.FizzBuzz, error)
	Healthz() error
}
