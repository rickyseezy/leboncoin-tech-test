# Base image
FROM golang:alpine3.13 as base

ENV GOPATH /go
ENV GO111MODULE on
ENV GOSUMDB off

RUN apk add build-base

WORKDIR /go/src/leboncoin.tech/test

COPY . .

# modd
RUN go get github.com/cortesi/modd/cmd/modd

RUN go mod download

ENTRYPOINT ["modd"]

EXPOSE 8000

# builder
FROM base as builder

RUN go build -v -o app ./cmd/fizzbuzz

# runtime
FROM alpine:3.13.6 as runtime

# Switch working directory to /usr/bin
WORKDIR /usr/bin

# Copies the binary file from the BUILD container to /usr/bin
RUN mkdir -p /internal/configs
COPY --from=builder /go/src/leboncoin.tech/test/app .
COPY --from=builder /go/src/leboncoin.tech/test/internal/configs/prod.config.yml .

# Exposes port 80 from the container
EXPOSE 80

ENTRYPOINT ["/usr/bin/app"]
