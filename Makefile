## Alias
DOCKER_TEST=docker-compose -f docker-compose.test.yml
DOCKER_PROD=docker-compose -f docker-compose.prod.yml
APP_DIR=./internal/...

## To start up
start:
	docker-compose up app

start@prod:
	$(DOCKER_PROD) up app

stop:
	docker-compose down

stop@prod:
	$(DOCKER_PROD) down

## Linter
lint:
	golangci-lint run $(APP_DIR)

lint@fix:
	golangci-lint run $(APP_DIR) --fix

## Generate mock interface
generate:
	go generate ./...

## Tests
test@run:
	$(DOCKER_TEST) run --rm unit_tests

test@unit:
	mkdir -p reports
	go test ./internal/... -coverprofile=./reports/coverage.out
	go tool cover -html=./reports/coverage.out -o ./reports/cover.html
	go tool cover -func=./reports/coverage.out
