package main

import (
	"github.com/gin-gonic/gin"
	"github.com/globalsign/mgo"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"leboncoin.tech/test/internal/application"
	"leboncoin.tech/test/internal/commons/environments"
	"leboncoin.tech/test/internal/commons/messages"
	"leboncoin.tech/test/internal/configs"
	"leboncoin.tech/test/internal/datasources"
	"leboncoin.tech/test/internal/interfaces/rest"
)

func main() {
	c := configs.New()

	session, err := mgo.Dial(c.Database.URI)
	if err != nil {
		log.Fatalln(errors.Wrap(err, messages.DatabaseNetworkError))
	}
	defer session.Close()

	fizzBuzzRepo := datasources.New(session, c)
	fizzBuzzApp := application.New(fizzBuzzRepo)

	var r *gin.Engine
	if c.Server.Env == environments.Prod {
		gin.SetMode(gin.ReleaseMode)
		r = gin.New()
		r.Use(gin.Recovery())
	} else {
		r = gin.Default()
	}

	s := rest.New(r, fizzBuzzApp, c.Server.Port)
	s.Start()
}
