# LeBonCoin - FizzBuzz Server

LeBonCoin technical assessment. 

## Installation

Run the command below to install dependencies
```bash
go mod download
```

## Environments

Available environments
- Dev
- Prod
- Test (used to run unit tests only)

```bash
# development environment
make start
# production environment
make start@prod
```

## Endpoints

Server should start at `localhost:8080`. Full documentation is available [here](https://documenter.getpostman.com/view/15511705/U16ooNxU). 

- /healthz
- /fizzbuzz
- /stats

## Unit tests

Test are run using [Docker](https://www.docker.com/)
```bash
make test@run
```

## Linter

Make sure to install [golangci-lint](https://golangci-lint.run/usage/install/), then run the following command.
```bash
make lint
```
or this command to fix source code automatically.

```bash
make lint@fix
```

## Third-party packages 

- Testify [Testify](https://github.com/stretchr/testify)
- GoMockGen  [GoMockGen](https://github.com/golang/mock)
- Logrus  [Logrus](https://github.com/sirupsen/logrus)
- GlobalSign [GlobalSign](https://github.com/globalsign)
- Ozzo-validation [Ozzo-validation](https://github.com/go-ozzo/ozzo-validation)
